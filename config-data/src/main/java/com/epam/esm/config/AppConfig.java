package com.epam.esm.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.AbstractEnvironment;
import org.springframework.core.io.ClassPathResource;

/**
 * App Config class for creating beans using ComponentScan annotation
 * For properties placeholder connecting with application.properties
 *
 * @author Abduvohid Isroilov
 */
@Configuration
@ComponentScan(basePackages = "com.epam.esm")
public class AppConfig {

    @Value("${spring.profiles.active}")
    private String activeProfile;

    private static final Logger LOG = LoggerFactory.getLogger(AppConfig.class);

    /**
     * Configuring PropertySourcesPlaceholderConfigurer for the providing connection with application.properties
     *
     * @return PropertySourcesPlaceholderConfigurer
     */
    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        String profile = System.getProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME);
        PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer = new PropertySourcesPlaceholderConfigurer();
        propertySourcesPlaceholderConfigurer.setLocations(new ClassPathResource("application-" + profile + ".properties"));
        propertySourcesPlaceholderConfigurer.setIgnoreUnresolvablePlaceholders(true);
        propertySourcesPlaceholderConfigurer.setIgnoreResourceNotFound(true);
        return propertySourcesPlaceholderConfigurer;
    }

}
