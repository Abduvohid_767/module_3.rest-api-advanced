package com.epam.esm.utils;

import com.epam.esm.exception.BadRequestException;
import com.epam.esm.exception.handling.ApiErrorMessages;
import org.springframework.data.domain.Sort;

import java.util.List;

public class OrdersHelper {

    public void settingOrders(String[] sort, List<Sort.Order> orders) {
        try {

            if (sort[0].contains(",")) {
                for (String sortOrder : sort) {
                    String[] _sort = sortOrder.split(",");
                    orders.add(new Sort.Order(Sort.Direction.fromString(_sort[1]), _sort[0]));
                }
            } else
                orders.add(new Sort.Order(Sort.Direction.fromString(sort[1]), sort[0]));
        } catch (IllegalArgumentException ex) {
            throw new BadRequestException(ApiErrorMessages.BAD_REQUEST + " ( " + ex.getMessage() + " ) ");
        }
    }
}
