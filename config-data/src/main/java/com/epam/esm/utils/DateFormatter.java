package com.epam.esm.utils;

import com.epam.esm.exception.BadRequestException;
import com.epam.esm.exception.handling.ApiErrorMessages;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class DateFormatter {


    private final SimpleDateFormat DATE_TIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public Timestamp parseTimestamp(String timestamp) {
        try {
            return new Timestamp(DATE_TIME_FORMAT.parse(timestamp).getTime());
        } catch (ParseException e) {
            throw new BadRequestException(ApiErrorMessages.BAD_REQUEST + " ( " + e.getMessage() + " ) ");
        }
    }

    public String parseToString(Timestamp timestamp) {
        try {
            return String.valueOf(timestamp);
        } catch (Exception ex) {
            throw new IllegalArgumentException(ex);
        }
    }
}
