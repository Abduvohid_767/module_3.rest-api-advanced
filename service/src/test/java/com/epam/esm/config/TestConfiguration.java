//package com.epam.esm.config;
//
//import com.epam.esm.dao.GiftCertificateDaoImpl;
//import com.epam.esm.dao.TagDaoImpl;
//import com.epam.esm.database.GiftCertificateQueries;
//import com.epam.esm.database.TagQueries;
//import com.epam.esm.mapper.GiftCertificateMapper;
//import com.epam.esm.mapper.GiftCertificateObjectMapper;
//import com.epam.esm.mapper.TagMapper;
//import com.epam.esm.mapper.TagObjectObjectMapper;
//import com.epam.esm.services.GiftCertificateService;
//import com.epam.esm.services.TagService;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.core.io.ClassPathResource;
//import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.jdbc.datasource.DriverManagerDataSource;
//import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
//import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
//import org.springframework.jdbc.datasource.init.DataSourceInitializer;
//import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
//
//import javax.annotation.PostConstruct;
//import javax.sql.DataSource;
//
//@Configuration
//public class TestConfiguration {
//
//    @PostConstruct
//    public void init() {
//        System.out.println("Test Config is configured !");
//    }
//
//    @Bean
//    public GiftCertificateService giftCertificateService() {
//        return new GiftCertificateService(giftCertificateDaoImpl(), giftCertificateObjectMapper());
//    }
//
//    @Bean
//    public GiftCertificateObjectMapper giftCertificateObjectMapper() {
//        return new GiftCertificateObjectMapper();
//    }
//
//    @Bean
//    public TagObjectObjectMapper tagObjectObjectMapper() {
//        return new TagObjectObjectMapper();
//    }
//
//    @Bean
//    public TagService tagService() {
//        return new TagService(tagDaoImpl(), tagObjectObjectMapper());
//    }
//
//    @Bean
//    public GiftCertificateDaoImpl giftCertificateDaoImpl() {
//        return new GiftCertificateDaoImpl(giftCertificateQueries(), giftCertificateMapper(), jdbcTemplate());
//    }
//
//    @Bean
//    public TagDaoImpl tagDaoImpl() {
//        return new TagDaoImpl(tagQueries(), tagMapper(), jdbcTemplate());
//    }
//
//    @Bean
//    public TagMapper tagMapper() {
//        return new TagMapper();
//    }
//
//    @Bean
//    public TagQueries tagQueries() {
//        return new TagQueries();
//    }
//
//    @Bean
//    public GiftCertificateMapper giftCertificateMapper() {
//        return new GiftCertificateMapper();
//    }
//
//    @Bean
//    public GiftCertificateQueries giftCertificateQueries() {
//        return new GiftCertificateQueries();
//    }
//
//    @Bean
//    public JdbcTemplate jdbcTemplate() {
//        return new JdbcTemplate(h2Datasource());
//    }
//
//    /**
//     * Initializing h2  test database
//     *
//     * @return DataSourceInitializer
//     */
//
//    @Bean
//    public DataSourceInitializer dataSourceInitializer() {
//
//        ResourceDatabasePopulator resourceDatabasePopulator = new ResourceDatabasePopulator();
//        resourceDatabasePopulator.addScript(new ClassPathResource("jdbc/schema.sql"));
//
//        DataSourceInitializer dataSourceInitializer = new DataSourceInitializer();
//        dataSourceInitializer.setDataSource(h2Datasource());
//        dataSourceInitializer.setDatabasePopulator(resourceDatabasePopulator);
//        return dataSourceInitializer;
//    }
//
//    /**
//     * Giving datasource  parameters for h2 test database
//     *
//     * @return DataSource
//     */
//    private DataSource dataSource() {
//        DriverManagerDataSource dataSource = new DriverManagerDataSource();
//
//        dataSource.setDriverClassName("org.h2.Driver");
//        dataSource.setUrl("jdbc:h2:mem:epam-esm_test");
//        dataSource.setUsername("sa");
//        dataSource.setPassword("password");
//
//        return dataSource;
//    }
//
//    /**
//     * Giving datasource parameters for h2 embedded test database automatic
//     *
//     * @return DataSource
//     */
//    @Bean
//    public DataSource h2Datasource() {
//        return new EmbeddedDatabaseBuilder().setType(EmbeddedDatabaseType.H2).build();
//    }
//
//}