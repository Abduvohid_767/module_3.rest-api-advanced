//package com.epam.esm;
//
//import com.epam.esm.config.TestConfiguration;
//import com.epam.esm.dto.GiftCertificateDto;
//import com.epam.esm.exception.CustomNotFoundException;
//import com.epam.esm.model.GiftCertificate;
//import com.epam.esm.services.GiftCertificateService;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mockito.MockitoAnnotations;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
//
//import java.util.*;
//
//import static org.junit.jupiter.api.Assertions.*;
//
///**
// * @author Abduvohid Isroilov
// * Testing Gift certificate service layer
// * ApplicationContext will be loaded using @SpringJUnitConfig
// */
//@SpringJUnitConfig(TestConfiguration.class)
//public class GiftCertificateServiceTest {
//
//    /**
//     * Injecting GiftCertificateService
//     */
//    @Autowired
//    GiftCertificateService giftCertificateService;
//
//    /**
//     * This method will run before every @Test method in this class
//     * Creating some gift certificate when this method is starting
//     */
//    @BeforeEach
//    public void setup() {
//
//        MockitoAnnotations.initMocks(this);
//
//        List<GiftCertificateDto> giftCertificates = Arrays.asList(
//                new GiftCertificateDto(1L, "gift1", "1", 12.3, 3, "2018-08-29 06:12:15.156", "2018-08-29 06:12:15.156", new ArrayList<>()),
//                new GiftCertificateDto(2L, "gift2", "2", 12.3, 3, "2018-08-29 06:12:15.156", "2018-08-29 06:12:15.156", new ArrayList<>())
//        );
//
//        giftCertificateService.create(giftCertificates.get(0));
//        giftCertificateService.create(giftCertificates.get(1));
//
//
//    }
//
//    /**
//     * Testing GiftCertificateService's getAll() method
//     */
//    @Test
//    public void testingGiftCertificateGetAll() {
//        Collection<GiftCertificateDto> giftCertificates = giftCertificateService.getAll();
//        assertFalse(giftCertificates.isEmpty());
//    }
//
//    /**
//     * Testing GiftCertificateService's create() method
//     */
//    @Test
//    public void testingCreatingGiftCertificate() {
//
//        GiftCertificateDto giftCertificate = new GiftCertificateDto(3L, "gift3", "3", 12.3, 3, "2018-08-29 06:12:15.156", "2018-08-29 06:12:15.156", new ArrayList<>());
//        giftCertificateService.create(giftCertificate);
//        GiftCertificateDto checkingObject = giftCertificateService.getById(3L);
//        assertNotNull(checkingObject);
//        assertEquals(checkingObject.getDescription(), "3");
//    }
//
//    /**
//     * Testing GiftCertificateService's getById() method
//     */
//    @Test
//    public void testingGiftCertificateGetById() {
//        GiftCertificateDto giftCertificate = giftCertificateService.getById(1L);
//        assertNotNull(giftCertificate);
//        assertEquals(giftCertificate.getDescription(), "1");
//    }
//
//    /**
//     * Testing GiftCertificateService's deleteById() method
//     */
//    @Test
//    public void testingGiftCertificateDeleteById() {
//        giftCertificateService.deleteById(2L);
//        assertThrows(CustomNotFoundException.class, () -> giftCertificateService.getById(2L));
//    }
//
//    /**
//     * Testing GiftCertificateService's updateById() method
//     */
//    @Test
//    public void testingGiftCertificateUpdateById() {
//
//        GiftCertificateDto giftCertificate = new GiftCertificateDto(2L, "gift2", "2_UPDATED", 12.3, 3, "2018-08-29 06:12:15.156", "2018-08-29 06:12:15.156", new ArrayList<>());
//        giftCertificateService.updateGiftCertificateById(giftCertificate, 2L);
//        GiftCertificateDto checkingObject = giftCertificateService.getById(2L);
//        assertNotNull(checkingObject);
//        assertEquals(checkingObject.getDescription(), "2_UPDATED");
//
//    }
//}
