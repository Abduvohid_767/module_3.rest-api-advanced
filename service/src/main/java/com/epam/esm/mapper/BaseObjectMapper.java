//package com.epam.esm.mapper;
//
//import com.epam.esm.dto.BaseDto;
//import com.epam.esm.model.BaseEntity;
//
///**
// * Base Mapper for the converting from dto to model objects and vice versa.
// * M should extends BaseModel, D should extends BaseDto
// *
// * @param <M>
// * @param <D>
// * @author Abduvohid Isroilov
// */
//public interface BaseObjectMapper<M extends BaseEntity, D extends BaseDto> {
//
//    /**
//     * Dto to Model converter method
//     *
//     * @param dto DTO
//     * @return MODEL
//     */
//    M dtoToModel(D dto);
//
//    /**
//     * Model to Dto converter method
//     *
//     * @param model MODEL
//     * @return DTO
//     */
//    D modelToDto(M model);
//
//}
