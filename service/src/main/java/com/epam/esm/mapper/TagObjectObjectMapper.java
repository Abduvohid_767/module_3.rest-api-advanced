//package com.epam.esm.mapper;
//
//import com.epam.esm.dto.GiftCertificateDto;
//import com.epam.esm.dto.TagDto;
//import com.epam.esm.model.GiftCertificate;
//import com.epam.esm.model.Tag;
//import org.springframework.stereotype.Component;
//
//import java.util.LinkedList;
//import java.util.List;
//
///**
// * Tag Object mapper for the converting tag model to dto
// * And vice versa
// *
// * @author Abduvohid Isroilov
// */
//@Component
//public class TagObjectObjectMapper implements BaseObjectMapper<Tag, TagDto> {
//
//    /**
//     * @param dto
//     * @return Tag
//     * @value TagDto
//     */
//    @Override
//    public Tag dtoToModel(TagDto dto) {
//        GiftCertificateObjectMapper giftCertificateObjectMapper = new GiftCertificateObjectMapper();
//
//        Tag tag = new Tag();
//
//        tag.setName(dto.getName());
//
//        if (dto.getId() != null && dto.getId() != 0)
//            tag.setId(dto.getId());
//
//        List<GiftCertificate> giftCertificates = new LinkedList<>();
//
//        dto.getGiftCertificateDtos()
//                .forEach(giftCertificateDto -> {
//                    if (giftCertificateDto != null)
//                        giftCertificates.add(giftCertificateObjectMapper.dtoToModel(giftCertificateDto));
//                });
//
//        if (!giftCertificates.isEmpty())
//            tag.setGiftCertificates(giftCertificates);
//
//        return tag;
//    }
//
//    /**
//     * @param model
//     * @return TagDto
//     * @value Tag
//     */
//    @Override
//    public TagDto modelToDto(Tag model) {
//
//        TagDto tagDto = new TagDto();
//        GiftCertificateObjectMapper giftCertificateObjectMapper = new GiftCertificateObjectMapper();
//
//        if (model.getId() != null && model.getId() != 0) {
//            tagDto.setId(model.getId());
//        }
//
//        tagDto.setName(model.getName());
//
//        List<GiftCertificateDto> giftCertificateDtos = new LinkedList<>();
//
//        model.getGiftCertificates()
//                .forEach(giftCertificate -> {
//                    if (giftCertificate != null)
//                        giftCertificateDtos.add(giftCertificateObjectMapper.modelToDto(giftCertificate));
//                });
//
//        if (!giftCertificateDtos.isEmpty())
//            tagDto.setGiftCertificateDtos(giftCertificateDtos);
//
//        return tagDto;
//    }
//
//}
