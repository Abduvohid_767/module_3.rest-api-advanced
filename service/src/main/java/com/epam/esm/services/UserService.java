package com.epam.esm.services;

import com.epam.esm.dto.UserDto;
import com.epam.esm.mapping.UserMapper;
import com.epam.esm.model.User;
import com.epam.esm.repo.UserRepository;
import com.epam.esm.specs.UserSpecs;
import org.springframework.stereotype.Service;

@Service
public class UserService extends BaseService<UserRepository, UserMapper, UserSpecs, User, UserDto> {

    public UserService(UserRepository repository, UserMapper mapper, UserSpecs specs) {
        super(repository, mapper, specs);
    }

}
