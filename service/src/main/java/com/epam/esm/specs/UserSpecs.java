package com.epam.esm.specs;

import com.epam.esm.model.User;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class UserSpecs extends BaseSpecs<User> {

    private Set<String> phoneNumbers = new HashSet<>();
    private Set<String> firstNames = new HashSet<>();
    private Set<String> lastNames = new HashSet<>();
    private Set<String> addresses = new HashSet<>();
    private Set<String> ages = new HashSet<>();

    public Set<String> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(Set<String> phoneNumbers) {
        if (phoneNumbers != null)
            this.phoneNumbers.addAll(phoneNumbers);
    }

    public Set<String> getFirstNames() {
        return firstNames;
    }

    public void setFirstNames(Set<String> firstNames) {
        if (firstNames != null)
            this.firstNames.addAll(firstNames);
    }

    public Set<String> getLastNames() {
        return lastNames;
    }

    public void setLastNames(Set<String> lastNames) {
        if (lastNames != null)
            this.lastNames.addAll(lastNames);
    }

    public Set<String> getAddresses() {
        return addresses;
    }

    public void setAddresses(Set<String> addresses) {
        if (addresses != null)
            this.addresses.addAll(addresses);
    }

    public Set<String> getAges() {
        return ages;
    }

    public void setAges(Set<String> ages) {
        if (ages != null)
            this.ages.addAll(ages);
    }

    @Override
    public Predicate toPredicateChild(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

        List<Predicate> predicates = new ArrayList<>(5);

        if (!phoneNumbers.isEmpty()) {
            predicates.add(root.get("phoneNumber").in(phoneNumbers));
        }

        if (!firstNames.isEmpty()) {
            predicates.add(root.get("firstName").in(firstNames));
        }

        if (!lastNames.isEmpty()) {
            predicates.add(root.get("lastName").in(lastNames));
        }

        if (!addresses.isEmpty()) {
            predicates.add(root.get("address").in(addresses));
        }

        if (!ages.isEmpty()) {
            predicates.add(root.get("age").in(ages));
        }


        query.distinct(true);

        return cb.and(predicates.toArray(new Predicate[predicates.size()]));
    }

}
