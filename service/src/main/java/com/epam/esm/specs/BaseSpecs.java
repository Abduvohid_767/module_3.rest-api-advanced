package com.epam.esm.specs;

import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * Base Specification class for filtering by class fields
 *
 * @param <E>
 * @author Abduvohid Isroilov
 */
public abstract class BaseSpecs<E> implements Specification<E> {

    @Override
    public Predicate toPredicate(Root<E> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

        List<Predicate> predicates = new ArrayList<>();
        // Applying child predicates and adding it
        Predicate childPredicate = toPredicateChild(root, query, cb);
        predicates.add(childPredicate);

        query.distinct(true);

        return cb.and(predicates.toArray(new Predicate[predicates.size()]));
    }

    public abstract Predicate toPredicateChild(Root<E> root, CriteriaQuery<?> query, CriteriaBuilder cb);

}
