package com.epam.esm.controller;

import com.epam.esm.dto.GiftCertificateDto;
import com.epam.esm.dto.PagedResult;
import com.epam.esm.dto.UserDto;
import com.epam.esm.dto.UserGiftCertificateDto;
import com.epam.esm.exception.DatabaseException;
import com.epam.esm.exception.handling.ApiErrorMessages;
import com.epam.esm.services.UserGiftCertificateService;
import com.epam.esm.services.UserService;
import com.epam.esm.specs.UserSpecs;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping("/user")
public class UserController {

    private final UserService userService;
    private final UserGiftCertificateService userGiftCertificateService;

    public UserController(UserService userService, UserGiftCertificateService userGiftCertificateService) {
        this.userService = userService;
        this.userGiftCertificateService = userGiftCertificateService;
    }

    /**
     * Getting all users
     *
     * @return ResponseEntity<?>
     */
    @GetMapping
    public ResponseEntity<?> get(
            @RequestParam(value = "phoneNumber", required = false) Set<String> phoneNumbers,
            @RequestParam(value = "firstName", required = false) Set<String> firstNames,
            @RequestParam(value = "lastName", required = false) Set<String> lastNames,
            @RequestParam(value = "address", required = false) Set<String> addresses,
            @RequestParam(value = "age", required = false) Set<String> ages,
            Pageable pageable
    ) {

        UserSpecs userSpecs = new UserSpecs();
        userSpecs.setPhoneNumbers(phoneNumbers);
        userSpecs.setFirstNames(firstNames);
        userSpecs.setLastNames(lastNames);
        userSpecs.setAddresses(addresses);
        userSpecs.setAges(ages);

        Page<UserDto> dtos = userService.getAllByPagination(userSpecs, pageable);

        setHateoasLinks(dtos);

        PagedResult pagedResult = getPagedResult(dtos);

        return new ResponseEntity<>(pagedResult, HttpStatus.OK);

    }

    private void setHateoasLinks(Page<UserDto> dtos) {
        dtos.getContent()
                .forEach(dto -> {
                    Link selfLink = linkTo(UserController.class).withSelfRel();
                    dto.add(selfLink);
                    Link dtoLink = linkTo(methodOn(UserController.class)
                            .getById(dto.getId())).withRel("user");
                    dto.add(dtoLink);
//                    if (dto.getGiftCertificates().size() > 0) {
//                        dto.getGiftCertificates().forEach(giftCertificateDto -> {
//                            Link tagLink = linkTo(methodOn(GiftCertificateController.class).getById(giftCertificateDto.getId())).withRel("gift_certificate");
//                            dto.add(tagLink);
//                            Link certificatesLink = linkTo(methodOn(GiftCertificateController.class)
//                                    .get(new HashSet<>(), new HashSet<>(), new HashSet<>(), new HashSet<>(), new HashSet<>(), new HashSet<>(), PageRequest.of(0, 1))).withRel("gift_certificates");
//                            dto.add(certificatesLink);
//                        });
//                    }
                });
    }

    private PagedResult getPagedResult(Page<UserDto> dtos) {
        PagedResult pagedResult = new PagedResult();
        pagedResult.buildMetadataFrom(dtos);
        pagedResult.setItems(dtos.getContent());
        return pagedResult;
    }

    /**
     * Getting User by its id
     *
     * @param id @PathVariable
     * @return ResponseEntity<?>
     */
    @GetMapping("/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id) {

        UserDto userDto = userService.getById(id);

        Link selfLink = linkTo(methodOn(UserController.class)
                .getById(id)).withSelfRel();

        Link getAllLink = linkTo(methodOn(UserController.class)
                .get(new HashSet<>(), new HashSet<>(), new HashSet<>(), new HashSet<>(), new HashSet<>(), PageRequest.of(0, 1))).withRel("users");

        EntityModel<UserDto> result = EntityModel.of(userDto, selfLink, getAllLink);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    /**
     * Creating new User
     *
     * @param userDto @PathVariable
     * @return ResponseEntity<?>
     */
    @PostMapping
    public ResponseEntity<?> create(@RequestBody UserDto userDto) {
        try {
            return new ResponseEntity<>(userService.create(userDto), HttpStatus.CREATED);
        } catch (Exception ex) {
            throw new DatabaseException(ApiErrorMessages.INTERNAL_SERVER_ERROR + " ( " + ex.getMessage() + " ) ");
        }
    }

    /**
     * Making gift certificate for the User
     *
     * @return ResponseEntity<?>
     */
    @PostMapping("/{user_id}/certificate/{certificate_id}")
    public ResponseEntity<?> buyCertificate(@PathVariable Long user_id, @PathVariable Long certificate_id) {
        return new ResponseEntity<>(userGiftCertificateService.buyCertificate(user_id, certificate_id), HttpStatus.CREATED);
    }

    @GetMapping("/{user_id}/certificates")
    public ResponseEntity<?> getUserOrders(@PathVariable Long user_id) {
        List<UserGiftCertificateDto> dtos = userGiftCertificateService.getAllUserOrders(user_id);

        dtos.forEach(dto -> {
            Link selfLink = linkTo(methodOn(GiftCertificateController.class).getById(dto.getGiftCertificate().getId())).withRel("gift_certificate");
            dto.add(selfLink);
            Link userLink = linkTo(methodOn(UserController.class).getById(user_id)).withSelfRel();
            dto.add(userLink);
        });

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping("/most/{id}")
    public ResponseEntity<?> getMostWidely(@PathVariable Long id) {
        return new ResponseEntity<>(userGiftCertificateService.getMaxTagAndHighestCost(id), HttpStatus.OK);
    }

}
