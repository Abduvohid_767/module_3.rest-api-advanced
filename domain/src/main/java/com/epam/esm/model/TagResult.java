package com.epam.esm.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TagResult {

    @JsonProperty("name")
    private String name;

    @JsonProperty("id")
    private Long id;

    @JsonProperty("max_cost")
    private Double maxCost;

    public TagResult() {
    }

    public TagResult(String name, Long id, Double maxCost) {
        this.name = name;
        this.id = id;
        this.maxCost = maxCost;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getMaxCost() {
        return maxCost;
    }

    public void setMaxCost(Double maxCost) {
        this.maxCost = maxCost;
    }
}
