package com.epam.esm.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "user")
public class User extends BaseEntity {

    @Column(name = "phone_number")
    private String phoneNumber;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "address")
    private String address;
    @Column(name = "age")
    private String age;

    @OneToMany(cascade = {CascadeType.DETACH, CascadeType.REFRESH}, mappedBy = "giftCertificate", orphanRemoval = true)
    private Set<UserGiftCertificate> giftCertificates = new HashSet<>();

    public User() {
    }

    public User(Long id, String phoneNumber, String firstName, String lastName, String address, String age, Set<UserGiftCertificate> giftCertificates) {
        super(id);
        this.phoneNumber = phoneNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.age = age;
        this.giftCertificates = giftCertificates;
    }

    public User(String phoneNumber, String firstName, String lastName, String address, String age, Set<UserGiftCertificate> giftCertificates) {
        this.phoneNumber = phoneNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.age = age;
        this.giftCertificates = giftCertificates;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public Set<UserGiftCertificate> getGiftCertificates() {
        return giftCertificates;
    }

    public void setGiftCertificates(Set<UserGiftCertificate> giftCertificates) {
        this.giftCertificates = giftCertificates;
    }

    public void addCertificate(List<UserGiftCertificate> giftCertificateList) {
        if (this.giftCertificates != null)
            this.giftCertificates.addAll(giftCertificateList);
    }

}

