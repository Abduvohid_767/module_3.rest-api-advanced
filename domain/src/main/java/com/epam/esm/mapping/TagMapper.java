package com.epam.esm.mapping;

import com.epam.esm.dto.TagDto;
import com.epam.esm.model.Tag;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(
        componentModel = "spring",
        uses = {GiftCertificateMapper.class},
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE
)
public interface TagMapper extends BaseMapper<Tag, TagDto> {
}
