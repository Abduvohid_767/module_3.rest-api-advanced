package com.epam.esm.mapping;

import com.epam.esm.dto.BaseDto;
import com.epam.esm.model.BaseEntity;

import java.util.List;

public interface BaseMapper<E extends BaseEntity, D extends BaseDto> {

    E dtoToEntity(D dto);

    D entityToDto(E entity);

    List<D> entitiesToDtos(List<E> entities);

    List<E> dtosToEntities(List<D> dtos);

}
