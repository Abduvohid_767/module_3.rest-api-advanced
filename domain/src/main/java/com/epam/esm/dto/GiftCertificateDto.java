package com.epam.esm.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * This is the dto class for exchanging data with client and service
 *
 * @author Abduvohid Isroilov
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class GiftCertificateDto extends BaseDto {

    private String description;
    private Double price;
    private Integer duration;
    private String create_date;
    private String last_update_date;
    private List<TagDto> tags;
    private String name;

    /**
     * No argument constructor for giftCertificate dto class
     */
    public GiftCertificateDto() {
    }


    public GiftCertificateDto(String description, Double price, Integer duration, String create_date, String last_update_date, List<TagDto> tags, String name) {
        this.description = description;
        this.price = price;
        this.duration = duration;
        this.create_date = create_date;
        this.last_update_date = last_update_date;
        this.tags = tags;
        this.name = name;
    }

    public GiftCertificateDto(Long id, String description, Double price, Integer duration, String create_date, String last_update_date, List<TagDto> tags, String name) {
        super(id);
        this.description = description;
        this.price = price;
        this.duration = duration;
        this.create_date = create_date;
        this.last_update_date = last_update_date;
        this.tags = tags;
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getCreate_date() {
        return create_date;
    }

    public void setCreate_date(String create_date) {
        this.create_date = create_date;
    }

    public String getLast_update_date() {
        return last_update_date;
    }

    public void setLast_update_date(String last_update_date) {
        this.last_update_date = last_update_date;
    }

    public List<TagDto> getTags() {
        return tags;
    }

    public void setTags(List<TagDto> tags) {
        this.tags = tags;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "GiftCertificateDto{" +
                "description='" + description + '\'' +
                ", price=" + price +
                ", duration=" + duration +
                ", create_date='" + create_date + '\'' +
                ", last_update_date='" + last_update_date + '\'' +
                ", tags=" + tags +
                ", name='" + name + '\'' +
                ", id=" + getId() +
                '}';
    }
}
