package com.epam.esm.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.hateoas.RepresentationModel;

/**
 * Base model for the client connection
 *
 * @author Abduvohid Isroilov
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class BaseDto extends RepresentationModel<BaseDto> {

    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BaseDto() {
    }

    public BaseDto(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "BaseDto{" +
                "id=" + id +
                '}';
    }
}
