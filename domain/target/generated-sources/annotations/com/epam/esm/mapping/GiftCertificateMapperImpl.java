package com.epam.esm.mapping;

import com.epam.esm.dto.GiftCertificateDto;
import com.epam.esm.dto.TagDto;
import com.epam.esm.model.GiftCertificate;
import com.epam.esm.model.Tag;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-07-16T11:36:56+0500",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_292 (Private Build)"
)
@Component
public class GiftCertificateMapperImpl implements GiftCertificateMapper {

    @Autowired
    private TagMapper tagMapper;

    @Override
    public GiftCertificateDto entityToDto(GiftCertificate entity) {
        if ( entity == null ) {
            return null;
        }

        GiftCertificateDto giftCertificateDto = new GiftCertificateDto();

        if ( entity.getId() != null ) {
            giftCertificateDto.setId( entity.getId() );
        }
        if ( entity.getDescription() != null ) {
            giftCertificateDto.setDescription( entity.getDescription() );
        }
        if ( entity.getPrice() != null ) {
            giftCertificateDto.setPrice( entity.getPrice() );
        }
        if ( entity.getDuration() != null ) {
            giftCertificateDto.setDuration( entity.getDuration() );
        }
        if ( entity.getCreate_date() != null ) {
            giftCertificateDto.setCreate_date( new SimpleDateFormat().format( entity.getCreate_date() ) );
        }
        if ( entity.getLast_update_date() != null ) {
            giftCertificateDto.setLast_update_date( new SimpleDateFormat().format( entity.getLast_update_date() ) );
        }
        List<TagDto> list = tagSetToTagDtoList( entity.getTags() );
        if ( list != null ) {
            giftCertificateDto.setTags( list );
        }
        if ( entity.getName() != null ) {
            giftCertificateDto.setName( entity.getName() );
        }

        return giftCertificateDto;
    }

    @Override
    public List<GiftCertificateDto> entitiesToDtos(List<GiftCertificate> entities) {
        if ( entities == null ) {
            return null;
        }

        List<GiftCertificateDto> list = new ArrayList<GiftCertificateDto>( entities.size() );
        for ( GiftCertificate giftCertificate : entities ) {
            list.add( entityToDto( giftCertificate ) );
        }

        return list;
    }

    @Override
    public List<GiftCertificate> dtosToEntities(List<GiftCertificateDto> dtos) {
        if ( dtos == null ) {
            return null;
        }

        List<GiftCertificate> list = new ArrayList<GiftCertificate>( dtos.size() );
        for ( GiftCertificateDto giftCertificateDto : dtos ) {
            list.add( dtoToEntity( giftCertificateDto ) );
        }

        return list;
    }

    @Override
    public GiftCertificate giftCertificateUpdateFromGiftCertificateDto(GiftCertificateDto giftCertificateDto, GiftCertificate giftCertificate) {
        if ( giftCertificateDto == null ) {
            return null;
        }

        if ( giftCertificateDto.getCreate_date() != null ) {
            giftCertificate.setCreate_date( GiftCertificateMapper.stringToTimestamp( giftCertificateDto.getCreate_date() ) );
        }
        if ( giftCertificateDto.getLast_update_date() != null ) {
            giftCertificate.setLast_update_date( GiftCertificateMapper.stringToTimestamp( giftCertificateDto.getLast_update_date() ) );
        }
        if ( giftCertificateDto.getId() != null ) {
            giftCertificate.setId( giftCertificateDto.getId() );
        }
        if ( giftCertificate.getTags() != null ) {
            Set<Tag> set = tagDtoListToTagSet( giftCertificateDto.getTags() );
            if ( set != null ) {
                giftCertificate.getTags().clear();
                giftCertificate.getTags().addAll( set );
            }
        }
        else {
            Set<Tag> set = tagDtoListToTagSet( giftCertificateDto.getTags() );
            if ( set != null ) {
                giftCertificate.setTags( set );
            }
        }
        if ( giftCertificateDto.getDescription() != null ) {
            giftCertificate.setDescription( giftCertificateDto.getDescription() );
        }
        if ( giftCertificateDto.getPrice() != null ) {
            giftCertificate.setPrice( giftCertificateDto.getPrice() );
        }
        if ( giftCertificateDto.getDuration() != null ) {
            giftCertificate.setDuration( giftCertificateDto.getDuration() );
        }
        if ( giftCertificateDto.getName() != null ) {
            giftCertificate.setName( giftCertificateDto.getName() );
        }

        return giftCertificate;
    }

    @Override
    public GiftCertificate dtoToEntity(GiftCertificateDto dto) {
        if ( dto == null ) {
            return null;
        }

        GiftCertificate giftCertificate = new GiftCertificate();

        if ( dto.getCreate_date() != null ) {
            giftCertificate.setCreate_date( GiftCertificateMapper.stringToTimestamp( dto.getCreate_date() ) );
        }
        if ( dto.getLast_update_date() != null ) {
            giftCertificate.setLast_update_date( GiftCertificateMapper.stringToTimestamp( dto.getLast_update_date() ) );
        }
        if ( dto.getId() != null ) {
            giftCertificate.setId( dto.getId() );
        }
        Set<Tag> set = tagDtoListToTagSet( dto.getTags() );
        if ( set != null ) {
            giftCertificate.setTags( set );
        }
        if ( dto.getDescription() != null ) {
            giftCertificate.setDescription( dto.getDescription() );
        }
        if ( dto.getPrice() != null ) {
            giftCertificate.setPrice( dto.getPrice() );
        }
        if ( dto.getDuration() != null ) {
            giftCertificate.setDuration( dto.getDuration() );
        }
        if ( dto.getName() != null ) {
            giftCertificate.setName( dto.getName() );
        }

        return giftCertificate;
    }

    protected List<TagDto> tagSetToTagDtoList(Set<Tag> set) {
        if ( set == null ) {
            return null;
        }

        List<TagDto> list = new ArrayList<TagDto>( set.size() );
        for ( Tag tag : set ) {
            list.add( tagMapper.entityToDto( tag ) );
        }

        return list;
    }

    protected Set<Tag> tagDtoListToTagSet(List<TagDto> list) {
        if ( list == null ) {
            return null;
        }

        Set<Tag> set = new HashSet<Tag>( Math.max( (int) ( list.size() / .75f ) + 1, 16 ) );
        for ( TagDto tagDto : list ) {
            set.add( tagMapper.dtoToEntity( tagDto ) );
        }

        return set;
    }
}
