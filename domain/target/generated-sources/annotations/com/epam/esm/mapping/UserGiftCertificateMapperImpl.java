package com.epam.esm.mapping;

import com.epam.esm.dto.UserGiftCertificateDto;
import com.epam.esm.model.UserGiftCertificate;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-07-16T11:36:56+0500",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_292 (Private Build)"
)
@Component
public class UserGiftCertificateMapperImpl implements UserGiftCertificateMapper {

    @Autowired
    private GiftCertificateMapper giftCertificateMapper;

    @Override
    public UserGiftCertificate dtoToEntity(UserGiftCertificateDto dto) {
        if ( dto == null ) {
            return null;
        }

        UserGiftCertificate userGiftCertificate = new UserGiftCertificate();

        if ( dto.getId() != null ) {
            userGiftCertificate.setId( dto.getId() );
        }
        if ( dto.getGiftCertificate() != null ) {
            userGiftCertificate.setGiftCertificate( giftCertificateMapper.dtoToEntity( dto.getGiftCertificate() ) );
        }
        if ( dto.getPurchaseDate() != null ) {
            userGiftCertificate.setPurchaseDate( dto.getPurchaseDate() );
        }
        if ( dto.getCost() != null ) {
            userGiftCertificate.setCost( dto.getCost() );
        }

        return userGiftCertificate;
    }

    @Override
    public UserGiftCertificateDto entityToDto(UserGiftCertificate entity) {
        if ( entity == null ) {
            return null;
        }

        UserGiftCertificateDto userGiftCertificateDto = new UserGiftCertificateDto();

        if ( entity.getId() != null ) {
            userGiftCertificateDto.setId( entity.getId() );
        }
        if ( entity.getGiftCertificate() != null ) {
            userGiftCertificateDto.setGiftCertificate( giftCertificateMapper.entityToDto( entity.getGiftCertificate() ) );
        }
        if ( entity.getPurchaseDate() != null ) {
            userGiftCertificateDto.setPurchaseDate( entity.getPurchaseDate() );
        }
        if ( entity.getCost() != null ) {
            userGiftCertificateDto.setCost( entity.getCost() );
        }

        return userGiftCertificateDto;
    }

    @Override
    public List<UserGiftCertificateDto> entitiesToDtos(List<UserGiftCertificate> entities) {
        if ( entities == null ) {
            return null;
        }

        List<UserGiftCertificateDto> list = new ArrayList<UserGiftCertificateDto>( entities.size() );
        for ( UserGiftCertificate userGiftCertificate : entities ) {
            list.add( entityToDto( userGiftCertificate ) );
        }

        return list;
    }

    @Override
    public List<UserGiftCertificate> dtosToEntities(List<UserGiftCertificateDto> dtos) {
        if ( dtos == null ) {
            return null;
        }

        List<UserGiftCertificate> list = new ArrayList<UserGiftCertificate>( dtos.size() );
        for ( UserGiftCertificateDto userGiftCertificateDto : dtos ) {
            list.add( dtoToEntity( userGiftCertificateDto ) );
        }

        return list;
    }
}
