package com.epam.esm.mapping;

import com.epam.esm.dto.UserDto;
import com.epam.esm.dto.UserGiftCertificateDto;
import com.epam.esm.model.User;
import com.epam.esm.model.UserGiftCertificate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-07-16T11:36:56+0500",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_292 (Private Build)"
)
@Component
public class UserMapperImpl implements UserMapper {

    @Autowired
    private GiftCertificateMapper giftCertificateMapper;

    @Override
    public User dtoToEntity(UserDto dto) {
        if ( dto == null ) {
            return null;
        }

        User user = new User();

        if ( dto.getId() != null ) {
            user.setId( dto.getId() );
        }
        if ( dto.getPhoneNumber() != null ) {
            user.setPhoneNumber( dto.getPhoneNumber() );
        }
        if ( dto.getFirstName() != null ) {
            user.setFirstName( dto.getFirstName() );
        }
        if ( dto.getLastName() != null ) {
            user.setLastName( dto.getLastName() );
        }
        if ( dto.getAddress() != null ) {
            user.setAddress( dto.getAddress() );
        }
        if ( dto.getAge() != null ) {
            user.setAge( dto.getAge() );
        }
        Set<UserGiftCertificate> set = userGiftCertificateDtoSetToUserGiftCertificateSet( dto.getGiftCertificates() );
        if ( set != null ) {
            user.setGiftCertificates( set );
        }

        return user;
    }

    @Override
    public UserDto entityToDto(User entity) {
        if ( entity == null ) {
            return null;
        }

        UserDto userDto = new UserDto();

        if ( entity.getId() != null ) {
            userDto.setId( entity.getId() );
        }
        if ( entity.getPhoneNumber() != null ) {
            userDto.setPhoneNumber( entity.getPhoneNumber() );
        }
        if ( entity.getFirstName() != null ) {
            userDto.setFirstName( entity.getFirstName() );
        }
        if ( entity.getLastName() != null ) {
            userDto.setLastName( entity.getLastName() );
        }
        if ( entity.getAddress() != null ) {
            userDto.setAddress( entity.getAddress() );
        }
        if ( entity.getAge() != null ) {
            userDto.setAge( entity.getAge() );
        }
        Set<UserGiftCertificateDto> set = userGiftCertificateSetToUserGiftCertificateDtoSet( entity.getGiftCertificates() );
        if ( set != null ) {
            userDto.setGiftCertificates( set );
        }

        return userDto;
    }

    @Override
    public List<UserDto> entitiesToDtos(List<User> entities) {
        if ( entities == null ) {
            return null;
        }

        List<UserDto> list = new ArrayList<UserDto>( entities.size() );
        for ( User user : entities ) {
            list.add( entityToDto( user ) );
        }

        return list;
    }

    @Override
    public List<User> dtosToEntities(List<UserDto> dtos) {
        if ( dtos == null ) {
            return null;
        }

        List<User> list = new ArrayList<User>( dtos.size() );
        for ( UserDto userDto : dtos ) {
            list.add( dtoToEntity( userDto ) );
        }

        return list;
    }

    protected UserGiftCertificate userGiftCertificateDtoToUserGiftCertificate(UserGiftCertificateDto userGiftCertificateDto) {
        if ( userGiftCertificateDto == null ) {
            return null;
        }

        UserGiftCertificate userGiftCertificate = new UserGiftCertificate();

        if ( userGiftCertificateDto.getId() != null ) {
            userGiftCertificate.setId( userGiftCertificateDto.getId() );
        }
        if ( userGiftCertificateDto.getGiftCertificate() != null ) {
            userGiftCertificate.setGiftCertificate( giftCertificateMapper.dtoToEntity( userGiftCertificateDto.getGiftCertificate() ) );
        }
        if ( userGiftCertificateDto.getPurchaseDate() != null ) {
            userGiftCertificate.setPurchaseDate( userGiftCertificateDto.getPurchaseDate() );
        }
        if ( userGiftCertificateDto.getCost() != null ) {
            userGiftCertificate.setCost( userGiftCertificateDto.getCost() );
        }

        return userGiftCertificate;
    }

    protected Set<UserGiftCertificate> userGiftCertificateDtoSetToUserGiftCertificateSet(Set<UserGiftCertificateDto> set) {
        if ( set == null ) {
            return null;
        }

        Set<UserGiftCertificate> set1 = new HashSet<UserGiftCertificate>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        for ( UserGiftCertificateDto userGiftCertificateDto : set ) {
            set1.add( userGiftCertificateDtoToUserGiftCertificate( userGiftCertificateDto ) );
        }

        return set1;
    }

    protected UserGiftCertificateDto userGiftCertificateToUserGiftCertificateDto(UserGiftCertificate userGiftCertificate) {
        if ( userGiftCertificate == null ) {
            return null;
        }

        UserGiftCertificateDto userGiftCertificateDto = new UserGiftCertificateDto();

        if ( userGiftCertificate.getId() != null ) {
            userGiftCertificateDto.setId( userGiftCertificate.getId() );
        }
        if ( userGiftCertificate.getGiftCertificate() != null ) {
            userGiftCertificateDto.setGiftCertificate( giftCertificateMapper.entityToDto( userGiftCertificate.getGiftCertificate() ) );
        }
        if ( userGiftCertificate.getPurchaseDate() != null ) {
            userGiftCertificateDto.setPurchaseDate( userGiftCertificate.getPurchaseDate() );
        }
        if ( userGiftCertificate.getCost() != null ) {
            userGiftCertificateDto.setCost( userGiftCertificate.getCost() );
        }

        return userGiftCertificateDto;
    }

    protected Set<UserGiftCertificateDto> userGiftCertificateSetToUserGiftCertificateDtoSet(Set<UserGiftCertificate> set) {
        if ( set == null ) {
            return null;
        }

        Set<UserGiftCertificateDto> set1 = new HashSet<UserGiftCertificateDto>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        for ( UserGiftCertificate userGiftCertificate : set ) {
            set1.add( userGiftCertificateToUserGiftCertificateDto( userGiftCertificate ) );
        }

        return set1;
    }
}
