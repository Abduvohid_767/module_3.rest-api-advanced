package com.epam.esm.mapping;

import com.epam.esm.dto.TagDto;
import com.epam.esm.model.Tag;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-07-16T11:36:56+0500",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_292 (Private Build)"
)
@Component
public class TagMapperImpl implements TagMapper {

    @Override
    public Tag dtoToEntity(TagDto dto) {
        if ( dto == null ) {
            return null;
        }

        Tag tag = new Tag();

        if ( dto.getId() != null ) {
            tag.setId( dto.getId() );
        }
        if ( dto.getName() != null ) {
            tag.setName( dto.getName() );
        }

        return tag;
    }

    @Override
    public TagDto entityToDto(Tag entity) {
        if ( entity == null ) {
            return null;
        }

        TagDto tagDto = new TagDto();

        if ( entity.getId() != null ) {
            tagDto.setId( entity.getId() );
        }
        if ( entity.getName() != null ) {
            tagDto.setName( entity.getName() );
        }

        return tagDto;
    }

    @Override
    public List<TagDto> entitiesToDtos(List<Tag> entities) {
        if ( entities == null ) {
            return null;
        }

        List<TagDto> list = new ArrayList<TagDto>( entities.size() );
        for ( Tag tag : entities ) {
            list.add( entityToDto( tag ) );
        }

        return list;
    }

    @Override
    public List<Tag> dtosToEntities(List<TagDto> dtos) {
        if ( dtos == null ) {
            return null;
        }

        List<Tag> list = new ArrayList<Tag>( dtos.size() );
        for ( TagDto tagDto : dtos ) {
            list.add( dtoToEntity( tagDto ) );
        }

        return list;
    }
}
