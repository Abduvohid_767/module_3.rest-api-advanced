package com.epam.esm.repo;

import com.epam.esm.model.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends BaseRepository<User> {
}
