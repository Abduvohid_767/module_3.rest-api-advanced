package com.epam.esm.repo;

import com.epam.esm.model.BaseEntity;
import com.epam.esm.model.GiftCertificate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

@NoRepositoryBean
public interface BaseRepository<E extends BaseEntity> extends JpaRepository<E, Long> {

    Page<E> findAll(Specification<E> specs, Pageable pageable);

    List<E> findAll(Specification<E> specs);


}
