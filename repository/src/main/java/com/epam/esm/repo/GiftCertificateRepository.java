package com.epam.esm.repo;

import com.epam.esm.model.GiftCertificate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface GiftCertificateRepository extends BaseRepository<GiftCertificate> {

    Optional<GiftCertificate> findByName(String name);

    void deleteByName(String name);

    List<GiftCertificate> findAllByDescription(String description);

    Page<GiftCertificate> getAllByName(String name, Pageable pageable);

}
