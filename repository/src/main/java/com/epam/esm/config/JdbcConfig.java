package com.epam.esm.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

/**
 * Creating JdbcTemplate using JdbcDatasourceConfig class parameters
 *
 * @author Abduvohid Isroilov
 */
@Configuration
public class JdbcConfig {

    private static final Logger LOG = LoggerFactory.getLogger(JdbcConfig.class);

    /**
     * Creating jdbcTemplate and get it
     *
     * @param dataSource Datasource
     * @return JdbcTemplate
     */
    @Bean(name = "epam-esm")
    public JdbcTemplate jdbcTemplate(@Qualifier("epam-esm-datasource") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

}
