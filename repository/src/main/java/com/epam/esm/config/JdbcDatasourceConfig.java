package com.epam.esm.config;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.init.DatabasePopulator;
import org.springframework.jdbc.datasource.init.DatabasePopulatorUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;

import javax.sql.DataSource;

/**
 * Jdbc Config class created for the connection with mysql database
 * Inject DatabaseConfigData class where all static fields are defined
 *
 * @author Abduvohid Isroilov
 */
@Configuration
public class JdbcDatasourceConfig {

    private static final Logger LOG = LoggerFactory.getLogger(JdbcDatasourceConfig.class);

    private final DatabaseConfigData databaseConfigData;

    public JdbcDatasourceConfig(DatabaseConfigData databaseConfigData) {
        this.databaseConfigData = databaseConfigData;
    }

    /**
     * Jdbc datasource setting for @Primary database resources
     *
     * @return Datasource
     */
    @Bean(name = "epam-esm-datasource")
    @Primary
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();

        dataSource.setDriverClassName(databaseConfigData.getDriverClassName());
        dataSource.setUrl(databaseConfigData.getUrl());
        dataSource.setUsername(databaseConfigData.getUsername());
        dataSource.setPassword(databaseConfigData.getPassword());

//        Resource initGiftCertificateData = new ClassPathResource("static/db/GIFT_CERTIFICATE_DATA.csv");
//        Resource initGiftCertificateTags = new ClassPathResource("static/db/GIFT_CERTIFICATES_TAGS.csv");
//        Resource initTagData = new ClassPathResource("static/db/TAG_DATA.csv");
//        Resource initUserData = new ClassPathResource("static/db/USER_DATA.csv");
//        Resource initUserOrdersData = new ClassPathResource("static/db/USER_ORDERS_DATA.csv");
        Resource initHibernateSequenceData = new ClassPathResource("static/db/HIBERNATE_SEQUENCE.csv");
//
//        DatabasePopulator databasePopulator = new ResourceDatabasePopulator(
//        initGiftCertificateData, initGiftCertificateTags, initTagData, initUserData, initUserOrdersData,
//                initHibernateSequenceData);
//        DatabasePopulatorUtils.execute(databasePopulator, dataSource);

        return dataSource;
    }

    /*
     If used embedded H2 database here simple sql scripts
     */
//    @Bean
//    public DataSource dataSource() {
//        return new EmbeddedDatabaseBuilder()
//                .setType(EmbeddedDatabaseType.H2)
//                .addScript("classpath:jdbc/schema.sql").build();
//    }

}