package com.epam.esm.database;

import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Base queries interface with all used database queries while exchanging data with database
 *
 * @param <T>
 * @author Abduvohid Isroilov
 */
@Component
public interface BaseQueries<T> {

    String getAll();

    String getById();

    String getByName();

    String save();

    String deleteById();

    String updateById(T t);

    String batchUpdate();

}
