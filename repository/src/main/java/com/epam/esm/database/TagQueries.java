package com.epam.esm.database;

import com.epam.esm.model.Tag;
import org.springframework.stereotype.Component;

/**
 * All used queries for the tag class
 *
 * @author Abduvohid Isroilov
 */
@Component
public class TagQueries implements BaseQueries<Tag> {

    @Override
    public String getAll() {
        return "SELECT t.id tag_id,t.name tag_name, gc.id, gc.name, gc.description, gc.price, gc.duration, gc.create_date, gc.last_update_date from tag t" +
                "\nleft join gift_certificate_tags gct on gct.tag_id=t.id " +
                "\nleft join gift_certificate gc on gc.id = gct.gift_certificate_id";
    }

    @Override
    public String getById() {
        return "SELECT t.id tag_id,t.name tag_name, gc.id, gc.name, gc.description, gc.price, gc.duration, gc.create_date, gc.last_update_date from tag t" +
                "\nleft join gift_certificate_tags gct on gct.tag_id=t.id " +
                "\nleft join gift_certificate gc on gc.id = gct.gift_certificate_id where t.id = ?";
    }

    @Override
    public String getByName() {
        return "SELECT t.id tag_id,t.name tag_name, gc.id, gc.name, gc.description, gc.price, gc.duration, gc.create_date, gc.last_update_date from tag t" +
                "\nleft join gift_certificate_tags gct on gct.tag_id=t.id " +
                "\nleft join gift_certificate gc on gc.id = gct.gift_certificate_id where t.name = ?";
    }

    @Override
    public String save() {
        return " INSERT INTO tag (name) VALUES (?)";
    }

    @Override
    public String deleteById() {
        return "DELETE FROM tag WHERE id = ?";
    }

    @Override
    public String updateById(Tag tag) {
        return null;
    }

    @Override
    public String batchUpdate() {
        return null;
    }
}
