package com.epam.esm.mapper;

import com.epam.esm.model.BaseEntity;
import org.springframework.jdbc.core.RowMapper;


/**
 * BaseMapper class for map coming data from database to java object (T)
 *
 * @author Abduvohid Isroilov
 */
//public interface BaseMapper<T extends BaseEntity> extends RowMapper<T> {
//
//}
