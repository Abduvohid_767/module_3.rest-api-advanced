//package com.epam.esm.dao;
//
//import com.epam.esm.database.BaseQueries;
//import com.epam.esm.exception.CustomNotFoundException;
//import com.epam.esm.exception.DatabaseException;
//import com.epam.esm.exception.handling.ApiErrorMessages;
//import com.epam.esm.mapper.BaseMapper;
//import com.epam.esm.model.BaseEntity;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.Collection;
//import java.util.Optional;
//
///**
// * Base Dao for the data exchange with database
// * Implements common methods
// *
// * @param <T>
// * @param <Q>
// * @param <M>
// * @author Abduvohid Isroilov
// */
//public abstract class BaseDaoImpl<T extends BaseEntity, Q extends BaseQueries<T>, M extends BaseMapper<T>> implements BaseDao<T> {
//
//    private Q queries;
//    private M mapper;
//
//    public BaseDaoImpl(Q queries, M mapper, JdbcTemplate jdbcTemplate) {
//        this.queries = queries;
//        this.mapper = mapper;
//        this.jdbcTemplate = jdbcTemplate;
//    }
//
//    public Q getQueries() {
//        return queries;
//    }
//
//    public M getMapper() {
//        return mapper;
//    }
//
//    public JdbcTemplate getJdbcTemplate() {
//        return jdbcTemplate;
//    }
//
//    @Qualifier("epam-esm")
//    private final JdbcTemplate jdbcTemplate;
//
//    public BaseDaoImpl(JdbcTemplate jdbcTemplate) {
//        this.jdbcTemplate = jdbcTemplate;
//    }
//
//    /**
//     * Get data by its id
//     *
//     * @param id
//     * @return Optional<T>
//     */
//    @Override
//    public Optional<T> get(long id) {
//        try {
//            return Optional.ofNullable(jdbcTemplate.queryForObject(queries.getById(), mapper, id));
//        } catch (Exception ex) {
//            throw new CustomNotFoundException(ApiErrorMessages.NOT_FOUND + " WITH ID ( " + id + " ) ");
//        }
//    }
//
//    /**
//     * Get data by its name
//     *
//     * @param name
//     * @return Optional<T>
//     */
//    @Override
//    public Optional<T> getByName(String name) {
//        try {
//
//            return Optional.ofNullable(jdbcTemplate.queryForObject(queries.getByName(), mapper, name));
//        } catch (Exception ex) {
//            throw new CustomNotFoundException(ApiErrorMessages.NOT_FOUND + " WITH NAME ( " + name + " ) ");
//
//        }
//    }
//
//    @Override
//    public Collection<T> getAll() {
//        return jdbcTemplate.query(queries.getAll(), mapper);
//    }
//
//    /**
//     * Deleting data by its id
//     *
//     * @param id
//     * @return boolean
//     */
//    @Override
//    @Transactional
//    public boolean delete(long id) {
//        try {
//
//            return jdbcTemplate.update(queries.deleteById(), id) == 1;
//        } catch (Exception ex) {
//            throw new DatabaseException(ApiErrorMessages.INTERNAL_SERVER_ERROR + " COULD NOT DELETE GIFT BY ID ( " + id + " ) ");
//        }
//    }
//
//    @Override
//    public abstract long save(T t);
//
//}
